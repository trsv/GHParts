#ifndef GAUTH_H
#define GAUTH_H

#include <QObject>
#include <QtNetwork/QtNetwork>
#include <QtNetworkAuth>

class GAuth : public QObject
{
    Q_OBJECT
public:
    explicit GAuth(QObject *parent = nullptr);
    virtual ~GAuth();

signals:
    void granted();

public slots:
    static QString auth();

private slots:

    QString do_auth();

    QString get_access_token();
    void set_access_token(const QString & access_token);

    QString get_refresh_token();
    void set_refresh_token(const QString & refresh_token);

    QDateTime get_expires();
    void set_expires(const QDateTime & expires);
    void set_expires(const int expires_in);

    void grant();

    void get_data();
    void set_data();

    bool get_credentials();
    void refresh_token();

private:
    bool m_has_credentials = false;

    const QString m_path_to_resources;
    QString m_access_token;
    QString m_refresh_token;

    QUrl m_authUri;
    QString m_clientId;
    QUrl m_tokenUri;
    QString m_clientSecret;


    QDateTime m_expires;

    QOAuth2AuthorizationCodeFlow * m_google = nullptr;
    QOAuthHttpServerReplyHandler * m_replyHandler = nullptr;
};

#endif // GAUTH_H
