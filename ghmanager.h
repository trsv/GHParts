#ifndef GHMANAGER_H
#define GHMANAGER_H

#include <QObject>
#include <QEventLoop>
#include <QtNetwork>
#include <parser.h>

class GhManager: public QObject
{
public:
    explicit GhManager(QObject *parent = 0);
    virtual ~GhManager(){}
public slots:
    void SetList(const QList<QString> & list)
    {
        m_list.clear();
        m_list = list;
    }
    void startWork();
    void request(const QString & url_, const bool & flagCookie = false);
    void requestLogin(const QString & url_, const bool & flagCookie = false);
    void requestAddPrice(const QString & url_, const bool & flagCookie = false);
private:
    QScopedPointer<QNetworkAccessManager> m_manager;
    QList<QNetworkCookie> m_listCookie, m_listCookie2;
    QEventLoop m_loop;
    QList<QString> m_list;
    QString m_price_token;
    Parser m_parser;
};

#endif // GHMANAGER_H
