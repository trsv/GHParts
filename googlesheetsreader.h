#ifndef GOOGLESHEETSREADER_H
#define GOOGLESHEETSREADER_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>


class GoogleSheetsReader : public QObject
{

    Q_OBJECT
public:
    explicit GoogleSheetsReader(const QString & access_token, QNetworkAccessManager * network = nullptr, QObject *parent = nullptr);
    ~GoogleSheetsReader();

signals:

public slots:
    void set_access_token(const QString & token);

    QJsonObject read(const QString &range, const QString & sheet, const QString & document);
    QStringList read_column_g(const QString & sheet, const QString & document);

    //    void getParsingInfo(parsingInfo &info, const QString &sheet_name, const QString & sheet_id);

private slots:
    QByteArray await_request(const QNetworkRequest & request);

private:
    QScopedPointer <QNetworkAccessManager> m_network;
    QEventLoop m_loop;

    QString m_access_token;

};

#endif // GOOGLESHEETSREADER_H
