#include "googlesheetsreader.h"

#include <QJsonArray>
#include <QJsonValue>
#include <QDebug>


GoogleSheetsReader::GoogleSheetsReader(const QString &access_token, QNetworkAccessManager *network, QObject *parent) :
    QObject(parent)
{
    (network) ? m_network.reset(network) : m_network.reset(new QNetworkAccessManager(this));

    QObject::connect(m_network.data(), &QNetworkAccessManager::finished, &m_loop, &QEventLoop::quit);

    set_access_token(access_token);
}

GoogleSheetsReader::~GoogleSheetsReader()
{

}

QJsonObject GoogleSheetsReader::read(const QString &range, const QString &sheet, const QString &document)
{
    const auto url = QString("https://sheets.googleapis.com/v4/spreadsheets/%1/values/%2!%3?access_token=%4")
            .arg(document, sheet, range, m_access_token);

    const auto response = await_request(QNetworkRequest(QUrl(url)));

    if (response.isEmpty()) return QJsonObject();
    else return QJsonDocument::fromJson(response).object();
}

QByteArray GoogleSheetsReader::await_request(const QNetworkRequest &request)
{
    auto reply = m_network->get(request);
    m_loop.exec();

    reply->deleteLater();

    if(reply->error() == QNetworkReply::NoError) {
        const auto response = reply->readAll();
        return response;
    } else {
        qWarning() << "[GoogleSheetsReader] Reading error:" << reply->errorString() << reply->readAll();
        return QByteArray();
    }
}

void GoogleSheetsReader::set_access_token(const QString &token)
{
    m_access_token = token;
}

QStringList GoogleSheetsReader::read_column_g(const QString &sheet, const QString &document)
{
    const auto response = read("G2:G", sheet, document);
    if (!response.isEmpty()) {

        const auto values = response.value("values").toArray();

        QStringList value_list;
        for (const auto & row_raw : values) {
            const auto row = row_raw.toArray();

            // проверка на пустую ячейку
            if (row.isEmpty()) {
                value_list << "";
            } else {
                const auto row_value = row[0].toString();
                value_list << row_value;
            }
        }

        return value_list;

    } else {
        return QStringList();
    }


}

//void GoogleSheetsReader::getParsingInfo(parsingInfo &info, const QString &sheet_name, const QString &sheet_id)
//{
//    QJsonDocument result;
//    QJsonObject QJO;
//    QJsonArray items;
//    ProxyInfo pr;

//    if (read(result, sheet_name + "!B4:C10", sheet_id))
//    {
//        QJO = result.object();
//        items = QJO.value("values").toArray();

//        qDebug() << items;

//        for (int i=0;i<7;i++) {
//            if (items.size() > i && !items[i].toArray().isEmpty())
//            {
//                if (i>=0 && i<=2)
//                {
//                    pr.proxy = items[i].toArray()[0].toString();
//                    pr.type = items[i].toArray()[1].toString();
//                    info.proxyList.push_back(pr);
//                }
//                if (i==3)
//                {
//                    QString acBuff;
//                    acBuff =items[i].toArray()[0].toString();
//                    QStringList wordList = acBuff.split(":");
//                    info.acount.username = wordList[0];
//                    info.acount.password = wordList[1];
//                    info.acount.email = wordList[2];
//                    info.acount.epassword = wordList[3];
//                }

//                if (i==4) info.hashteg = items[i].toArray()[0].toString();
//                if (i==5) info.closedAcount = items[i].toArray()[0].toString();
//                if (i==6) info.driveFolder = items[i].toArray()[0].toString();

//            }
//        }

//        read(result, sheet_name + "!B13:H22", sheet_id);
//        QJO = result.object();
//        items = QJO.value("values").toArray();

//        int post_url_cell_id = 13;
//        int post_report_cell_id = 3;
//        char last_result_clumn = 'G';

//        foreach (QJsonValue item, items) {
//            post itemPost;
//            itemPost.column_in_result_table = last_result_clumn++;
//            itemPost.url_cell_id = post_url_cell_id++;
//            itemPost.report_cell_id = post_report_cell_id++;
//            if(item.toArray().size() > 6) {
//                itemPost.link = item.toArray()[0].toString();
//                itemPost.beforDate = item.toArray()[1].toString();
//                itemPost.beforTime = item.toArray()[2].toString();
//                itemPost.afterDate = item.toArray()[3].toString();
//                itemPost.afterTime = item.toArray()[4].toString();
//                itemPost.base = item.toArray()[5].toString();
//                itemPost.limit = item.toArray()[6].toString();
//            }
//            else if(item.toArray().size() <= 6) {
//                itemPost.link = item.toArray()[0].toString();
//                itemPost.base = item.toArray()[5].toString();
//            }
//            info.postList.push_back(itemPost);
//        }

//        read(result, sheet_name + "!B25:C34", sheet_id);
//        QJO = result.object();
//        items = QJO.value("values").toArray();
//        parsingTime itemTime;
//        foreach (QJsonValue item, items) {
//            itemTime.date = item.toArray()[0].toString();
//            itemTime.time = item.toArray()[1].toString();
//            info.timeList.push_back(itemTime);
//        }

//        read(result, sheet_name + "!B37:C38", sheet_id);
//        QJO = result.object();
//        items = QJO.value("values").toArray();
//        info.outputSheet = items[0].toArray()[0].toString();
//        info.outputRange = items[1].toArray()[0].toString();

//        read(result, sheet_name + "!C1", sheet_id);
//        QJO = result.object();
//        items = QJO.value("values").toArray();
//        info.imageDate = items[0].toArray()[0].toString();

//        read(result, sheet_name + "!E1", sheet_id);
//        QJO = result.object();
//        items = QJO.value("values").toArray();
//        if(!items.isEmpty())
//            info.GMT = items[0].toArray()[0].toString();
//        else
//            info.GMT = "0+";
//    } else {
//        qWarning() << "[GoogleSheetsReader] Can`t read sheet" << sheet_name << "in document" << sheet_id;
//    }
//}
