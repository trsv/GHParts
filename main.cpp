#include "presenter.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto presenter = new Presenter();
    Q_UNUSED(presenter);

    return a.exec();
}
