#include "parser.h"
#include "def.h"

Parser::Parser()
{

}

QString Parser::GetValue(const QString & key, QString & strBuff)
{
    int indexStart = strBuff.indexOf(key);
    int indexEnd = 0;
    QString strResult;
    if(key == GET_TOKEN_KEY)
    {
        indexStart += key.length();
        indexEnd = strBuff.indexOf("\"", indexStart);
        strResult = strBuff.mid(indexStart, indexEnd - indexStart);
    }
    else
        return QString::null;
//    else if(key == GET_DATA_KEY || key == GET_HIDDEN_KEY)
//    {
//        indexStart += key.length();
//        indexEnd = strBuff.indexOf("\"", indexStart);
//        strResult = strBuff.mid(indexStart, indexEnd - indexStart);
//    }

    return strResult;
}
