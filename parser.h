#ifndef PARSER_H
#define PARSER_H
#include <QtCore>


class Parser
{
public:
    Parser();
    ~Parser(){}
public slots:
    QString GetValue(const QString & key, QString & strBuff);
};

#endif // PARSER_H
