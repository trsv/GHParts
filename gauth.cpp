#include "gauth.h"

#include <QDesktopServices>
#include <memory>

GAuth::GAuth(QObject *parent) :
    QObject(parent),
    m_path_to_resources(QCoreApplication::applicationDirPath() + "/resources")
{
    m_google = new QOAuth2AuthorizationCodeFlow;
    m_replyHandler = new QOAuthHttpServerReplyHandler(8080);

    connect(m_google, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, &QDesktopServices::openUrl);
    connect(m_replyHandler, &QOAuthHttpServerReplyHandler::tokensReceived, [=](const QVariantMap & a) {

        const QString access_token = a.value("access_token", "").toString();
        const QString refresh_token = a.value("refresh_token", "").toString();
        const int expires_in = static_cast<int>(a.value("expires_in").toDouble());

        if (!access_token.isEmpty() && !refresh_token.isEmpty()) {

            set_access_token(access_token);
            set_refresh_token(refresh_token);
            set_expires(expires_in);

            set_data();

            emit granted();
        }

    });

    m_google->setReplyHandler(m_replyHandler);

    get_data();

    m_has_credentials = get_credentials();
}

GAuth::~GAuth()
{
    m_replyHandler->deleteLater();
    m_google->deleteLater();
}

QString GAuth::get_access_token()
{
    return m_access_token;
}

void GAuth::set_access_token(const QString &access_token)
{
    m_access_token = access_token;
}

QString GAuth::get_refresh_token()
{
    return m_refresh_token;
}

void GAuth::set_refresh_token(const QString &refresh_token)
{
    m_refresh_token = refresh_token;
}

QDateTime GAuth::get_expires()
{
    return m_expires;
}

void GAuth::set_expires(const QDateTime &expires)
{
    m_expires = expires;
}

void GAuth::set_expires(const int expires_in)
{
    QDateTime current_datetime = QDateTime::currentDateTime();
    current_datetime = current_datetime.addSecs(expires_in);

    m_expires = current_datetime;
}

void GAuth::refresh_token()
{
    std::unique_ptr<QNetworkAccessManager> manager;
    manager.reset(new QNetworkAccessManager);

    QEventLoop loop;
    QNetworkRequest request;
    QJsonDocument result;
    request.setRawHeader("Host", "www.googleapis.com");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setUrl(QUrl(QString("https://www.googleapis.com/oauth2/v4/token")));
    QByteArray postData;
    postData.append("client_id=" + m_clientId.toUtf8() + "&");
    postData.append("client_secret=" + m_clientSecret.toUtf8() + "&");
    postData.append("refresh_token=" + m_refresh_token.toUtf8() + "&");
    postData.append("grant_type=refresh_token");
    auto reply = manager->post(request,postData);
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    try
    {
        if (reply->error() == QNetworkReply::NoError)
        {
            QString PAGE;
            PAGE = reply->readAll();
            result = QJsonDocument::fromJson(PAGE.toUtf8());

            m_access_token = result.object().value("access_token").toString();

            const int expires_in = static_cast<int>(result.object().value("expires_in").toDouble());
            set_expires(expires_in);

            emit granted();
        }
        else
        {
            const std::string str = reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    } catch (const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}


void GAuth::grant()
{
    m_google->setScope(
//                "https://www.googleapis.com/auth/drive.appdata "
//                "https://www.googleapis.com/auth/drive.file "
//                "https://www.googleapis.com/auth/drive "
//                "https://www.googleapis.com/auth/drive.appfolder "
                "https://www.googleapis.com/auth/spreadsheets");


    m_google->setAuthorizationUrl(m_authUri);
    m_google->setClientIdentifier(m_clientId);
    m_google->setAccessTokenUrl(m_tokenUri);
    m_google->setClientIdentifierSharedKey(m_clientSecret);

    qDebug() << "[GAuth] Make auth";
    m_google->grant();
}

void GAuth::get_data()
{
    QSettings s(m_path_to_resources + "/credentials_store.ini", QSettings::IniFormat);

    const QString access_token = s.value("access_token", "").toString();
    const QString refresh_token = s.value("refresh_token", "").toString();

    const QString expires_raw = s.value("expires", "").toString();
    const QDateTime expires = QDateTime::fromString(expires_raw, "dd.MM.yyyy hh:mm");

    set_access_token(access_token);
    set_refresh_token(refresh_token);
    set_expires(expires);
}

void GAuth::set_data()
{
    QSettings s(m_path_to_resources + "/credentials_store.ini", QSettings::IniFormat);

    s.setValue("access_token", get_access_token());
    s.setValue("refresh_token", get_refresh_token());
    s.setValue("expires", get_expires().toString("dd.MM.yyyy hh:mm"));
}

bool GAuth::get_credentials()
{
    QFile file(m_path_to_resources +"/credentials.json");

    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "[GAuth] File with credentials not found";
        return false;
    }

    const auto json_raw = file.readAll();
    file.close();

    const auto document = QJsonDocument::fromJson(json_raw);
    const auto object = document.object();
    const auto settingsObject = object["installed"].toObject();

    const QUrl authUri(settingsObject["auth_uri"].toString());
    const auto clientId = settingsObject["client_id"].toString();
    const QUrl tokenUri(settingsObject["token_uri"].toString());
    const auto clientSecret(settingsObject["client_secret"].toString());

    m_authUri = authUri;
    m_clientId = clientId;
    m_tokenUri = tokenUri;
    m_clientSecret = clientSecret;

    return true;
}

QString GAuth::auth()
{
    GAuth auth;
    return auth.do_auth();
}

QString GAuth::do_auth()
{
    const auto current_time = QDateTime::currentDateTime();

    if (m_access_token.isEmpty()) {

        qDebug() << "[GAuth] Getting access token...";

        QEventLoop loop;
        connect(this, &GAuth::granted, &loop, &QEventLoop::quit);

        this->grant();
        loop.exec();

        return this->get_access_token();

    } else if  (m_expires <= current_time) {

        qDebug() << "[GAuth] Refresh access token...";

        this->refresh_token();
        this->set_data();
        return this->get_access_token();

    } else {

        qDebug() << "[GAuth] Current access token is valid";

        return this->get_access_token();

    }
}
