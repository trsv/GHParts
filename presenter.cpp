#include "presenter.h"
#include "gauth.h"
#include "googlesheetsreader.h"
#include <ghmanager.h>

Presenter::Presenter(QObject *parent) : QObject(parent)
{
    qDebug() << "[Presenter] Auth google api...";

    while(true)
    {
        const auto token = GAuth::auth();
        GoogleSheetsReader google_reader(token);

        // читаем столбец весь G. читаемые границы можно изменить внутри метода
        const auto list = google_reader.read_column_g("на сайт GH", "1PIiQdp_dDWG1uFMwCbM0sKQaJdXfbfY5CDyHXzmZCiM");

        // вывод считаных данных
        GhManager manager;
        manager.SetList(list);
        manager.startWork();
        qDebug() << "Wait... 3 hour";
        QThread::sleep(10800);
    }

}
