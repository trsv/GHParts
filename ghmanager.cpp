#include "ghmanager.h"
#include "def.h"


GhManager::GhManager(QObject *parent):QObject(parent)
{
    m_manager.reset(new QNetworkAccessManager());
}

void GhManager::startWork()
{
    request("http://gh-parts.com.ua/user/login", true);
    requestLogin("http://gh-parts.com.ua/user/login_check", true);
    request("http://gh-parts.com.ua/admin/add_prices", false);
    requestAddPrice("http://gh-parts.com.ua/admin/add_prices", true);
}

void GhManager::request(const QString &url_, const bool &flagCookie)
{
    QUrl url(url_);
    if(!m_listCookie2.isEmpty())
        m_manager->cookieJar()->setCookiesFromUrl(m_listCookie2, url);
    QNetworkRequest request;
    request.setRawHeader("Host", url.host().toStdString().c_str());
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Cache-Control", "max-age=0");
    request.setRawHeader("Upgrade-Insecure-Requests", "1");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
    request.setUrl(QUrl(url));

    QNetworkReply * m_reply = m_manager->get(request);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();
    QString strBuf = m_reply->readAll();
    try
    {
         // проверяем что пришло в ответе
        if (m_reply->error() == QNetworkReply::NoError)
        {
            if(flagCookie)
                m_listCookie = qvariant_cast <QList <QNetworkCookie> > (m_reply->header(QNetworkRequest::SetCookieHeader));
            m_price_token = m_parser.GetValue(GET_TOKEN_KEY, strBuf);
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch (const std::exception & excep)
    {
        qDebug() << excep.what();
        QString errostr = excep.what();
    }
}

void GhManager::requestLogin(const QString &url_, const bool &flagCookie)
{
    QUrl url(url_);
    QNetworkRequest request;
    QByteArray arr;
    arr.append("_username=Bot-Parser&_password=Bot-Parser");
    request.setRawHeader("Host", url.host().toStdString().c_str());
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Content-Length", QString::number(arr.length()).toStdString().c_str());
    request.setAttribute(QNetworkRequest::RedirectionTargetAttribute, true);
    m_manager->cookieJar()->setCookiesFromUrl(m_listCookie, url);
    request.setUrl(QUrl(url));

    QNetworkReply * m_reply = m_manager->post(request, arr);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();
    QByteArray strBuf = m_reply->readAll();
    try
    {
         // проверяем что пришло в ответе
        if (m_reply->error() == QNetworkReply::NoError)
        {
            m_listCookie2 = qvariant_cast <QList <QNetworkCookie> > (m_reply->header(QNetworkRequest::SetCookieHeader));
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch (const std::exception & excep)
    {
        qDebug() << excep.what();
        QString errostr = excep.what();
    }
}

void GhManager::requestAddPrice(const QString &url_, const bool &flagCookie)
{
    QUrl url(url_);
    QNetworkRequest request;
    QByteArray arr;
    arr.append("add_prices[list]=");
    for(int i = 0; i < m_list.length(); i++)
    {
        arr.append(m_list[i]+"\r\n");
    }
    arr.append("&add_prices[_token]=" + m_price_token);
    request.setRawHeader("Host", url.host().toStdString().c_str());
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    request.setRawHeader("Content-Length", QString::number(arr.length()).toStdString().c_str());
    request.setAttribute(QNetworkRequest::RedirectionTargetAttribute, true);
    m_manager->cookieJar()->setCookiesFromUrl(m_listCookie2, url);
    request.setUrl(QUrl(url));

    QNetworkReply * m_reply = m_manager->post(request, arr);
    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();
    QByteArray strBuf = m_reply->readAll();
    try
    {
         // проверяем что пришло в ответе
        if (m_reply->error() == QNetworkReply::NoError)
        {
            qDebug() << "Load price succes";
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch (const std::exception & excep)
    {
        qDebug() << excep.what();
        QString errostr = excep.what();
    }
}
